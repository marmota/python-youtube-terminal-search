#!/usr/bin/env python
#Get yout API key at 
#https://developers.google.com/youtube/v3/getting-started

#reading search youtube API json response
#https://developers.google.com/youtube/v3/docs/search/list#usage
import sys
import requests
import json
import re
from subprocess import call

api_key = ''
#testing, if argument was submitted
try:
	sys.argv[1]
except:
	print "well, no arguments submitted, quitting!"
	quit()
else:
	arguments =" ".join(sys.argv[1:99])
	print 'Searching YouTube for ',arguments
	#quit()
	url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=25&q='+arguments+'&key='+api_key+''
	r = requests.get(url)
	x = r.text.encode('utf-8')
	y = json.loads(x)
	# the result is a Python dictionary:
	xxx=0
	lst = []
	for i in (y["items"]):
		#print i['snippet']
		#print i['id']
		try:
			videoId = i['id']['videoId']
		except:
			continue
		xxx+=1
		url = re.sub("^","https://youtu.be/",videoId)
		print xxx, i['snippet']['title']
		lst.append(url)

length = str(len(lst))
text = raw_input("Choose from 1 to "+length+": ")  # Python 2
play_url = lst[int(text)-1]
print play_url
#from subprocess import call
#call('mpv '+play_url, shell=True)
call('./send_to_s5_via_ssh.sh '+play_url, shell=True)
